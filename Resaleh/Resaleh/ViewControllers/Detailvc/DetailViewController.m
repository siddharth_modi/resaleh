//
//  DetailViewController.m
//  Resaleh
//
//  Created by siddharth on 10/18/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import "DetailViewController.h"
#import "AppDelegate.h"
#import <sqlite3.h>
#import <SVProgressHUD.h>
#import "ViewController.h"

@interface DetailViewController ()
{
    sqlite3 *database;
    NSString *LeftswipeNull;
    NSString *RightswipeNull;
    BOOL flag;
}
@end

@implementation DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSUserDefaults standardUserDefaults] setValue:@"NO" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    LeftswipeNull=@"";
    RightswipeNull=@"";
    [self check_FavouriteOrNot:_str_id];
    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];

    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_categoryBack where _id=%@",_str_id]];
    
    
    [self set_Data:_str_name :_str_Comment];
    
    _btn_search.layer.cornerRadius=_btn_search.frame.size.height/2;
    _btn_search.layer.masksToBounds=YES;
    _txtView_Comment.userInteractionEnabled = YES;
    
    _txtView_Comment.editable = NO;

    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];



//    self.view.backgroundColor = [UIColor colorWithPatternImage: [UIImage imageNamed:@"tiledBG"]];
//    
//    
//   _btn_search.layer.cornerRadius=_btn_search.frame.size.height/2;
//    _btn_search.layer.masksToBounds=YES;
//    
//    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_categoryBack where _id=%@",_str_id]];
//    NSNumberFormatter *formatter = [NSNumberFormatter new];
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger i = 0; i < 10; i++)
//    {
//        NSNumber *num = @(i);
//        _str_name = [_str_name stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
//    for (NSInteger i = 0; i < 10; i++)
//    {
//        NSNumber *num = @(i);
//        _str_Comment = [_str_Comment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
//    }
//    _lbl_navname.text=_str_name;
//    _lbl_details.text=_str_Comment;
//
//   
//    CGSize size;
//    
//    CGRect frm_lbl=_lbl_details.frame;
//    size=[self getLabelSize:_lbl_details];
//
//    frm_lbl.size.height=size.height-10;
//    _lbl_details.frame=frm_lbl;
//    NSLog(@"%@",_str_favourite);
//    
//    if ([_str_favourite isEqualToString:@"0"])
//    {
//        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
//        flag=NO;
//
//    }
//    else
//    {
//        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
//        flag=YES;
//
//    }
    // Do any additional setup after loading the view.
}
-(void)check_FavouriteOrNot:(NSString *)str_Id
{
    _str_favourite=[self FetchData:[NSString stringWithFormat:@"SELECT * FROM WebSite_categoryBack where _id=%@",_str_id]];
    NSLog(@"%@",_str_favourite);
    
    if ([_str_favourite isEqualToString:@"0"])
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
        flag=NO;
        
    }
    else
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
        flag=YES;
        
    }
}
-(void)set_Data:(NSString *)str_Title :(NSString *)str_Comment
{
    NSNumberFormatter *formatter = [NSNumberFormatter new];
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        str_Title = [str_Title stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    formatter.locale = [NSLocale localeWithLocaleIdentifier:@"ar"];
    for (NSInteger i = 0; i < 10; i++)
    {
        NSNumber *num = @(i);
        str_Comment = [str_Comment stringByReplacingOccurrencesOfString:num.stringValue withString:[formatter stringFromNumber:num]];
    }
    NSString *aux = [NSString stringWithFormat:@"<span style=\"font-family: HelveticaNeue; font-size: 17\">%@</span>",str_Comment];
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[aux dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    _txtView_Comment.attributedText=attrStr;
    _txtView_Comment.textAlignment=NSTextAlignmentRight;
    _lbl_navname.text=str_Title;
}

#pragma mark - Swipe Left Method height
-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSLog(@" Left swipe ");
    
    
    NSMutableDictionary *Dic_Data=[[NSMutableDictionary alloc] init];
    if (![LeftswipeNull isEqualToString:@"Yes"])
    {
        NSInteger metadataID=[_str_id integerValue]-1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
    
    
    Dic_Data=[self FetchSwipeData:[NSString stringWithFormat:@"SELECT _id, tittle, comment FROM WebSite_CategoryBack where _id =%@",_str_id]];
    if (Dic_Data.count!=0)
    {
        _str_id=[Dic_Data valueForKey:@"Category_ID"];
        _str_Comment=[Dic_Data valueForKey:@"Comment"];
        _str_name=[Dic_Data valueForKey:@"Title"];
        LeftswipeNull=@"";
        [self check_FavouriteOrNot:_str_id];
        
        [self set_Data:_str_name :_str_Comment];
    }
    else
    {
        LeftswipeNull=@"Yes";
        NSInteger metadataID=[_str_id integerValue]+1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
        
    }
    
    
    //Do what you want here
}
#pragma mark - Swipe Right Method height
-(void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    //Do what you want here
    NSLog(@" Right swipe ");
    if (![RightswipeNull isEqualToString:@"Yes"])
    {
        NSInteger metadataID=[_str_id integerValue]+1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
    NSMutableDictionary *Dic_Data=[[NSMutableDictionary alloc] init];
    
    Dic_Data=[self FetchSwipeData:[NSString stringWithFormat:@"SELECT _id, tittle, comment FROM WebSite_CategoryBack where _id =%@",_str_id]];
    
    if (Dic_Data.count!=0)
    {
        _str_id=[Dic_Data valueForKey:@"Category_ID"];
        _str_Comment=[Dic_Data valueForKey:@"Comment"];
        _str_name=[Dic_Data valueForKey:@"Title"];
        RightswipeNull=@"";
        [self check_FavouriteOrNot:_str_id];
        
        [self set_Data:_str_name :_str_Comment];
        
    }
    else
    {
        RightswipeNull=@"Yes";
        NSInteger metadataID=[_str_id integerValue]-1;
        _str_id=[NSString stringWithFormat:@"%ld",(long)metadataID];
    }
}
-(NSMutableDictionary *)FetchSwipeData:(NSString *)querySQL
{
    //    [SVProgressHUD show];
    //    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    
    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            // [SVProgressHUD dismiss];
            
            
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                NSString *str_Category;
                NSString *str_Title;
                NSString *str_MetadataID;
                
                char *fieldCategory = (char *) sqlite3_column_text(statement,0);
                str_Category = [[NSString alloc] initWithUTF8String: fieldCategory];
                char *fieldTitle = (char *) sqlite3_column_text(statement,1);
                str_Title = [[NSString alloc] initWithUTF8String: fieldTitle];
                char *fieldMetadata = (char *) sqlite3_column_text(statement,2);
                
                if (fieldMetadata==nil)
                {
                    str_MetadataID=@"";
                }
                else
                {
                    str_MetadataID = [[NSString alloc] initWithUTF8String: fieldMetadata];
                    
                }
                
                
                [dic setValue:str_Category forKey:@"Category_ID"];
                [dic setValue:str_Title forKey:@"Title"];
                [dic setValue:str_MetadataID forKey:@"Comment"];
                
            }
        }
        else
        {
            
            //   [SVProgressHUD dismiss];
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return dic;
}

#pragma mark - Get label height

-(CGSize)getLabelSize :(UILabel *)label
{
    CGSize size = [label.text boundingRectWithSize:CGSizeMake(203, MAXFLOAT)
                   
                                           options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName: label.font }context:nil].size;
    return size;
}
-(NSString *)FetchData:(NSString *)querySQL
{
    NSMutableDictionary *dic=[[NSMutableDictionary alloc] init];
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    NSString *str_result=[[NSString alloc] init];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {

        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            if( sqlite3_step(statement) == SQLITE_ROW )
            {
                char *field1 = (char *) sqlite3_column_text(statement,7);
               str_result = [[NSString alloc] initWithUTF8String: field1];
                NSLog(@"dictioary is %@",dic);
                [SVProgressHUD dismiss];
            }
            else
            {
                [SVProgressHUD dismiss];

                UIAlertController *alert=[UIAlertController alertControllerWithTitle:@"My Alert" message:@"Data is not Found" preferredStyle:UIAlertControllerStyleAlert];
                UIAlertAction *cancelAction = [UIAlertAction
                                               actionWithTitle:NSLocalizedString(@"Cancel", @"Cancel action")
                                               style:UIAlertActionStyleCancel
                                               handler:^(UIAlertAction *action)
                                               {
                                                   NSLog(@"Cancel action");
                                               }];
                
                UIAlertAction *okAction = [UIAlertAction
                                           actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                           style:UIAlertActionStyleDefault
                                           handler:^(UIAlertAction *action)
                                           {
                                               NSLog(@"OK action");
                                           }];
                
                [alert addAction:cancelAction];
                [alert addAction:okAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        }
        else
        {
            
            [SVProgressHUD dismiss];

            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
    return str_result;
}
-(void )UpdateData:(NSString *)querySQL
{
    [SVProgressHUD show];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];

    
    AppDelegate *appDelegateLocal = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    const char *dbpath = [[appDelegateLocal getdatabase] UTF8String];
    if (sqlite3_open(dbpath, &database) == SQLITE_OK)
        
    {
        
        NSLog(@"%@",querySQL);
        const char *sqlStatement = [querySQL UTF8String];
        
        sqlite3_stmt *statement;
        if( sqlite3_prepare_v2(database, sqlStatement, -1, &statement, NULL) == SQLITE_OK )
        {
            
            if (sqlite3_step(statement) != SQLITE_DONE)
            {
                
            }
            else
            {
                [SVProgressHUD dismiss];

                NSLog(@"Updated");
            }
        }
        else
        {
            
            NSLog( @"Failed from sqlite3_prepare_v2. Error is:  %s", sqlite3_errmsg(database) );
            
        }
        sqlite3_finalize(statement);
        sqlite3_close(database);
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btn_favourite:(id)sender
{
    if (flag ==NO)
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"BookMark"] forState:UIControlStateNormal];
        [self UpdateData:[NSString stringWithFormat:@"UPDATE WebSite_categoryBack SET favorite='1' WHERE _id=%@",_str_id]];
        flag=YES;
    }
    else
    {
        [_btn_favourite setImage:[UIImage imageNamed:@"unfavourite"] forState:UIControlStateNormal];
        [self UpdateData:[NSString stringWithFormat:@"UPDATE WebSite_categoryBack SET favorite='0' WHERE _id=%@",_str_id]];
        flag=NO;
    }
}
- (IBAction)btn_back:(id)sender
{
    [[NSUserDefaults standardUserDefaults] setValue:@"Yes" forKey:@"sidemenu"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    if ([_str_flag isEqualToString:@""])
    {
        ViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"ViewController"];
        
        [self.navigationController pushViewController:vc animated:YES];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (IBAction)btn_share:(id)sender
{
    NSString *test = @"www.makarem.ir";
//    NSString *shareString=[NSString stringWithFormat:@"%@,%@",_str_name,_str_Comment];
    NSString *shareString = test;
    NSArray *activityItems = [[NSArray alloc] initWithObjects:shareString, nil];
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    [self presentViewController:activityViewController animated:YES completion:nil];
}
@end
