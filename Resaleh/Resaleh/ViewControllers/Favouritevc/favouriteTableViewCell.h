//
//  favouriteTableViewCell.h
//  Resaleh
//
//  Created by siddharth on 10/25/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface favouriteTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lbl_height;

@end
