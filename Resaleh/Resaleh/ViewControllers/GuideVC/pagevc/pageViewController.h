//
//  pageViewController.h
//  Resaleh
//
//  Created by siddharth on 10/19/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface pageViewController : UIViewController
@property (assign, nonatomic) NSInteger index;

@property (weak, nonatomic) IBOutlet UIImageView *img_swipe;
@property (weak, nonatomic) IBOutlet UILabel *lbl_detial;
@end
