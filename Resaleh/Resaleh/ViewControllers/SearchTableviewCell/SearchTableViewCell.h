//
//  SearchTableViewCell.h
//  Resaleh
//
//  Created by siddharth on 10/21/16.
//  Copyright © 2016 qtm. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbl_title1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_comment1;
@property (weak, nonatomic) IBOutlet UILabel *lbl_lastname1;
@property (weak, nonatomic) IBOutlet UIImageView *img_pattern1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *comment_height;

@property (weak, nonatomic) IBOutlet UIImageView *img_background1;
@end
